# Twitter API

This API provides functionality for Twitter-like service.


## User management 
User in Twitter service must authen through Authen API.

* GET /api/user/        get all users
* GET /api/user/?sessionid={sessionid}      get user with sessionid
* GET /api/user/{id}      get user with id
* POST /api/user/       create new user

    payload: 
```Javascript
    {
        "UserID" : "{AuthenAPI user ID}",
        "Name": "{Twitter name}"
    }
```
*  PUT /api/user/{id}    update user with id

    payload: 
```Javascript
    {
        "UserID" : "{AuthenAPI user ID}",
        "Name": "{Twitter name}",
    }
```

* DELETE /api/user/{id}     delete user with id

## Following management

* GET /api/following/         get all following from all users
* GET /api/following/?sessionid={sessionid}         get all following from session. 
* GET /api/following/{id}     get following with id
* POST /api/following/?sessionid={sessionid}        create new following with  following id and session

    payload: any of User ID or name
```Javascript
    {
        "UserID": "{Tweet message}",
        "Name": "{User name}"
    }
```
* DELETE /api/session/{id}  remove following id

## Twitter management

* GET /api/twitter/?recent={recent}&sessionid={sessionid}         get all tweets , filter-able by recent date / session id
* GET /api/twitter/{id}     get tweet by id
* GET /api/twitter/timeline     get timeline
* POST /api/twitter/?sessionid={sessionid} 

    payload: 
```Javascript
    {
        "Message": "{Tweet message}",
    }
```